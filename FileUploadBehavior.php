<?php
    /*
     * FileUpload behavior
     * Version 1.0
     * Description : it simplifies file uploading with error handling.
     * Author : Florian Leleu
     * Contact : leleu.florian@gmail.com or https://florianleleu@wordpress.com
     * Copyright : Florian Leleu CC-BY-SA
     * 
     * How to use it? have a look at the readme.md file
     */ 

    class FileUploadBehavior extends ModelBehavior
    {
        /* 
         * dir => by default name of the model
         * size => maximum size allowed
         * field => field of the form to check
         * extensions => array of allowed extensions, by default accepts everything
         * dimension => width x height of the file (if an image)
         * length => filename length (without extension)
         */
        protected $dir;
        protected $size;
        protected $field = 'file';
        protected $extensions = array();
        protected $dimension = null;
        protected $length = 255;
 
        /*
         * MAIN_DIR => root folder of the dir folder (webroot/files)
         */
        public static $MAIN_DIR = "files";

        /*
         * Sets the behavior's attributes
         */
        function setup(Model $model, $settings = array())
        {
            //default dir
            $this->dir = $model->name;
            
            //default size
            $this->setDefaultSize();
            
            //set it all
            $this->hydrate($settings);
        }
        
        /*
         * Hydration method
         */
        protected function hydrate($settings)
        {
            foreach($settings as $key => $value)
            {
                $method = 'set'.ucfirst($key);
                if(method_exists($this, $method))
                {
                    $this->$method($value);
                }
            }
        }
        
        protected function setField($val)
        {
            $this->field = $val;
        }
        
        protected function setDir($val)
        {
            $this->dir = $val;
        }
        
        protected function setSize($val)
        {
            if(self::isStringSize($val))
            {
                $size = self::stringSizeToBytes($val);
                
                if($size > $this->size)
                {
                    trigger_error('FileUploadBehavior Error: '.$val.' is higher than the value of your upload_max_filesize in php.ini, change it in your configuration.', E_USER_WARNING);   
                }
                else
                {
                    $this->size = $size;
                }                
            }
            else
            {
                if(preg_match('#^[0-9]+$#', $val))
                {
                    if($val > $this->size)
                    {
                        trigger_error('FileUploadBehavior Error: '.$val.' is higher than the value of your upload_max_filesize in php.ini, change it in your configuration.', E_USER_WARNING);   
                    }
                    else
                    {
                        $this->size = $val;
                    } 
                }
                else
                {
                    trigger_error('FileUploadBehavior Error: '.$val.' is not a valid size (ex: 567 or 3k).', E_USER_WARNING);  
                }
            }
        }
        
        protected function setDimension($val)
        {
            if(preg_match('#^[0-9]+(x[0-9]+)*!?$#i', $val))
            {
                $this->dimension = mb_strtolower($val, 'UTF-8');
            }
            else
            {
                trigger_error('FileUploadBehavior Error: '.$val.' is not a valid dimension.', E_USER_WARNING);  
            }
        }
        
        protected function setExtensions($val)
        {
            if(is_array($val))
            {
                $this->extensions = $val;
            }
            else
            {
                trigger_error('FileUploadBehavior Error: extensions must be an array.', E_USER_WARNING);  
            } 
        }        

        protected function setLength($val)
        {
            if(preg_match('#^[0-9]+$#', $val))
            {
                $this->length = $val;
            }
            else
            {
                trigger_error('FileUploadBehavior Error: '.$val.' is not a valid length (ex: 255, 12, ...).', E_USER_WARNING);  
            }
        }
        
        /*
         * Checks if the file validates
         */
        public function beforeSave(Model $model)
        {
            $return = true;
            
            //if the field doesn't exist, means we don't try to upload a file
            if(isset($model->data[$model->alias][$this->field]))
            {
                //checks if a file was uploaded without errors
                if(is_array($model->data[$model->alias][$this->field])
                && $model->data[$model->alias][$this->field]['error'] == UPLOAD_ERR_OK )
                {
                    //checks size
                    if($model->data[$model->alias][$this->field]['size'] > $this->size)
                    {
                        $model->invalidate($this->field); 
                        $model->validationErrors[$this->field] = "The filesize is too big.";

                        $return = false; 
                    }
                     
                    //checks dimension
                    if($this->dimension)
                    {
                        $sign = $this->dimension[strlen($this->dimension) - 1];
                        $strict = ($sign == "!")?true:false;
                                                
                        //gets dimension of the file works if it's an image
                        list($width, $height) = @getimagesize($model->data[$model->alias][$this->field]['tmp_name']);
                        
                        //is an image?
                        if($width && $height)
                        {
                            //gets width and height of dimension
                            $dimension = explode("x", $this->dimension);
                            $di_width = $dimension[0];
                            $di_height = (isset($dimension[1]))?$dimension[1]:$dimension[0];
                            
                            if($strict)
                            {
                                //exact dimension
                                if($width != $di_width || $height != $di_height)
                                {
                                    $model->invalidate($this->field);
                                    $model->validationErrors[$this->field] = "Dimension of the file do not match, it must be exactly ($di_width x $di_height).";

                                    $return = false;
                                }     
                            }
                            else
                            {
                                //oversized?
                                if($width > $di_width || $height > $di_height)
                                {
                                    $model->invalidate($this->field);
                                    $model->validationErrors[$this->field] = "Dimension of the file do not match ($di_width x $di_height).";

                                    $return = false;
                                } 
                            }
                        }
                    }
                    
                    //gets extension
                    $arr = explode('.', $model->data[$model->alias][$this->field]['name']);
                    $ext = $arr[count($arr) - 1];
                    $ext = mb_strtolower($ext, 'UTF-8');

                    //gets only the name of the file
                    unset($arr[count($arr) - 1]);
                    $name = trim(implode(' ', $arr));

                    //checks length of the filename
                    if(mb_strlen($name, 'UTF-8') > $this->length)
                    {
                        //name too long
                        $model->invalidate($this->field);
                        $model->validationErrors[$this->field] = "The filename is too long ($this->length characters allowed).";

                        $return = false;                            
                    }
                    
                    //checks if extension is allowed
                    if(!empty($this->extensions) && !in_array($ext, $this->extensions))
                    {
                        //extension not allowed
                        $model->invalidate($this->field);
                        $model->validationErrors[$this->field] = "This extension $ext is not allowed.";

                        $return = false;                    
                    }
                    else
                    {
                        //sets it in the model for save
                        $model->data[$model->alias]['extension'] = $ext; 
                    }

                    //sets real_name, safe_name and tmp_name to the data structure
                    if(isset($model->data[$model->alias]['id']))
                    {
                        //if we edit we get the previous file's safe_name and extension
                        $m = $model->findById($model->id, array('safe_name', 'extension'));
                        $model->data[$model->alias]['previous_file'] = $m[$model->alias]['safe_name'] . '.' . $m[$model->alias]['extension'];
                        
                        //we keep the previous safe_name for the new file
                        $model->data[$model->alias]['safe_name'] = $m[$model->alias]['safe_name'];
                    }
                    else
                    {
                        //else we generate one
                        $model->data[$model->alias]['safe_name'] = self::getSafeUniqueName();
                    }
                    
                    $model->data[$model->alias]['real_name'] = $name;
                    $model->data[$model->alias]['tmp_name'] = $model->data[$model->alias][$this->field]['tmp_name'];
                }
                else
                {
                    $model->invalidate($this->field);
                    $model->validationErrors[$this->field] = "No file was sent.";

                    $return = false;     
                }
            }
            
            return $return;
        }
        
        /*
         * After the record is saved, save the file to the dir 
         */
        public function afterSave(Model $model, $created)
        {
            //if the field doesn't exist, means we don't try to upload a file
            if(isset($model->data[$model->alias][$this->field]))
            {
                $dir = WWW_ROOT . self::$MAIN_DIR . DS .$this->dir . DS;

                //creates the folder if it does not exist
                if(!is_dir($dir))
                {
                    mkdir($dir, 0777, true);
                }
                
                //we edit so we erase previous file
                if(!$created && isset($model->data[$model->alias]['previous_file']))
                {
                    $filename = $dir . $model->data[$model->alias]['previous_file'];
                    if(file_exists($filename))
                        @unlink($filename); 
                }

                //moves the file
                $filename = $dir . $model->data[$model->alias]['safe_name'] . '.' . $model->data[$model->alias]['extension'];
                @move_uploaded_file($model->data[$model->alias]['tmp_name'], $filename);
            }
        }
        
        /*
         * Deletes the file from the dir
         */
        public function beforeDelete(Model $model, $cascade = true) 
        {
            if($model->id)
            {
                $m = $model->findById($model->id, array('safe_name', 'extension'));
                
                if($m)
                {
                    $dir = WWW_ROOT . self::$MAIN_DIR . DS .$this->dir . DS;
                    
                    $filename = $dir . $m[$model->alias]['safe_name'] . '.' . $m[$model->alias]['extension'];

                    //erases the file
                    if(file_exists($filename))
                        @unlink($filename);  
                }
            }
            
            return true;
        }
        
        /*
         * Gives the url of the folder's file (good for displaying images, videos, ... )
         */
        public function getUrl()
        {
            //replaces DS by / (in case of windows)
            $dir =  strtr($this->dir, DS, '/');
            
            return Router::url("/") . self::$MAIN_DIR . '/' . $dir.'/'; 
        }
        
        /*
         * Sets the size to default from the php.ini configuration
         */
        protected function setDefaultSize()
        {
            $this->size = self::stringSizeToBytes(ini_get('upload_max_filesize'));
        }
        
        /*
         * Checks if a string is a size, i.e 9K, 1M, 4G
         */
        public static function isStringSize($string)
        {
            return in_array(strtolower($string[strlen($string)-1]), array('k', 'm', 'g', 't'));
        }
        
        /*
         * Turns a string size to bytes
         * 1K => 1024 bytes
         * 1M => 1048576 bytes
         * 1G => 1073741824 bytes
         */
        public static function stringSizeToBytes($size)
        {
            $size = trim($size);
            
            $val = substr($size, 0, strlen($size) - 1);
            $ext = strtolower($size[strlen($size)-1]);
            
            switch($ext)
            {
                case 't':
                    $val *= pow(1024, 4);
                    break;                
                case 'g':
                    $val *= pow(1024, 3);
                    break;
                case 'm':
                    $val *= pow(1024, 2);
                    break;
                case 'k':
                    $val *= 1024;
                    break;
            }
            
            return $val;
        }
        
        /*
         * Makes a safe and unique name to save the file
         * You can change it, but make sure the name will be unique !
         */
        public static function getSafeUniqueName()
        {
            return str_replace('.', '', uniqid('', true));
        }
    }
?>

