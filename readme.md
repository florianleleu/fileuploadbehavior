Florian Leleu CC-BY-SA

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

-- version --
FileUploadBehavior 1.0

-- More about it --
FileUploadBehavior is a behavior for CakePHP 2 dealing with file uploading.
It checks different parameters that you can set such as extensions, size,
dimension (if an image), name length and save your file with a safe and unique name.

-- How to --
*Copy the file in the Behavior folder (app/Model/Behavior).

*Also you need to create some fields for your table.
        
Example with a table called "blabla".

	CREATE TABLE IF NOT EXISTS `blabla` 
	(
	    `id` int(11) NOT NULL AUTO_INCREMENT,
	    `real_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
	    `safe_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
	    `extension` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
	    PRIMARY KEY (`id`)
	)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

The fields real_name, safe_name and extension are mandatory.

! Don't change the value of the safe_name, it should stay a varchar(255) !

*Then in your model, just set the actsAs like this,

	public $actsAs = array
	(
		'FileUpload' => array
		(
			'field' => 'my_file',
			'dimension' => '1280x800',
			'extensions' => array("jpg", "png"),
			'size' => '4M',
			'dir' => 'blabla',
			'length' => 255
		)
	);

Description of the fields, their types and default values

Name		Type				Default		Description

field		string				file		virtual field, the one for your file form.

dimension	mixed 				null		dimension of an image, if not an image won't be checked.
								example : 40x20, or simply 40 (if you want a square).
								You can put a ! at the end to enable the strict mode,
								it means the file must have the same dimension.
								ex: 40x20!, 100!

extensions	array of strings		array()		extensions you allow, if not set it accepts everything !

size		int or string size(2M, 14K)	php.ini config	allowed size, defaut set to upload_max_filesize directive.

dir		string				model's name	name of the directory you save the files for this model.

length		int				255		length of the real_name, be sure to set the same for the database 									and the behavior.

There is also one static attributes which is the parent's folder, you can change it in the behavior, but it must exist, and
have the rights 777, it's better to leave it.
$MAIN_DIR = "files";

A word about where the files are saved. They are save in the webroot, under MAIN_DIR and then under the dir field.
webroot/MAIN_DIR/dir/safe_name.extension

It gives this kind of path for the example we have, with
MAIN_DIR = files,
dir = YourModel

webroot/files/YourModel/509d373dc7310760130360.png

*Then in your form view, just set a field of file type with the name of the FileUpload field you set.
Let's say you use the field 'file' to upload your file, you just have to write this,

echo $this->Form->input('my_file', array('type' => 'file'));

! Don't forget to create a form for file upload !

*Also you can use the getUrl method of the behavior to get the url for your files. Let's see with an example.
Imagine you want to display your files, in this case images, in a view. You need their url to display them.
In your controller the index method, you can do,

	public function index()
	{
	    $bla = $this->YourModel->find('all');
	    $this->set('bla', $bla);

	    // There we get the url of YourModel and give it to the view
	    $this->set('dir', $this->YourModel->getUrl());
	}

And then in your view index.ctp,
       
	// $p here is one record of YourModel
       	<img src="<?php echo $dir.$p['YourModel']['safe_name'].'.'.$p['YourModel']['extension']; ?>" />

-- More --
*You can change the getSafeUniqueName method, just be sure to create a unique name, that's all.
So you can change the type in your database of the safe_name, which was a varchar(255), it sets very long,
in case of modification, it does not matter.

-- Future --
*Add resizing support, to change dimension of an image using GD.
*Multiupload
*own directory for each file
*dimension strict checking for width and height
